import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Home.vue'
const Login = () => import('views/Login')
const Home = () => import('views/Home')
const Welcome = () => import('views/home/Welcome')
const Users = () => import('views/home/user/Users')
const Rights = () => import('views/home/power/Rights')
const Roles = () => import('views/home/power/Roles')
const Cate = () => import('views/home/goods/Cate')
const Params = () => import('views/home/goods/Params')
const Goods = () => import('views/home/goods/List')
const Add = () => import('views/home/goods/Add')
const Order = () => import('views/home/order/Order')
const Report = () => import('views/home/report/Report')

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/home',
    component: Home,
    redirect: '/welcome',
    children: [
      {
        path: '/welcome',
        component: Welcome
      },
      {
        path: '/users',
        component: Users
      },
      {
        path: '/rights',
        component: Rights
      },
      {
        path: '/roles',
        component: Roles
      },
      {
        path: '/categories',
        component: Cate
      },
      {
        path: '/params',
        component: Params
      },
      {
        path :'/goods',
        component: Goods,
      },
      {
        path:'/goods/add',
        component: Add
      },
      {
        path: '/orders',
        component: Order
      },
      {
        path:'/reports',
        component: Report
      }
     
    ]
  }
]

const router = new VueRouter({
  routes,
  mode: 'history'
})
// 路由导航守卫
router.beforeEach((to, from, next) => {
  if (to.path === '/login') return next()
  const tokenStr = window.sessionStorage.getItem('token')
  if (!tokenStr) return next('/login')
  next()
})
export default router
